using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MastermindAPI.Repositories;

namespace MastermindAPI
{
    public class Startup
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            // Configuration des services
            services.AddScoped<IGameRepository, GameRepository>();
            services.AddControllers();
            // Autres services nécessaires

            // Configuration de la documentation de l'API (Swagger, OpenAPI, etc.)
            // Configuration de l'authentification et de l'autorisation si nécessaire
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // Configuration des options de production
                // app.UseExceptionHandler("/Error");
                // app.UseHsts();
            }

            app.UseRouting();

            // Configuration de l'authentification et de l'autorisation si nécessaire

            // Configuration de la documentation de l'API (Swagger, OpenAPI, etc.)

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
