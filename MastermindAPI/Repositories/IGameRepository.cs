namespace MastermindAPI.Repositories
{
    public interface IGameRepository
    {
        Game StartNewGame();
        Game GetGameById(int gameId);
        void UpdateGame(Game game);
        void DeleteGame(Game game);
    }
}
