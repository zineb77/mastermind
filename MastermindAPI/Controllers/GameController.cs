using Microsoft.AspNetCore.Mvc;
using MastermindAPI.Models;
using MastermindAPI.Repositories;

namespace MastermindAPI.Controllers
{
    [Route("api/games")]
    [ApiController]
    public class GameController : ControllerBase
    {
        private readonly IGameRepository _gameRepository;

        public GameController(IGameRepository gameRepository)
        {
            _gameRepository = gameRepository;
        }

        [HttpPost]
        public ActionResult<Game> StartNewGame()
        {
            Game game = _gameRepository.StartNewGame();
            return Ok(game);
        }

        [HttpGet("{gameId}")]
        public ActionResult<Game> GetGame(int gameId)
        {
            Game game = _gameRepository.GetGameById(gameId);
            if (game == null)
                return NotFound();

            return Ok(game);
        }

        [HttpPut("{gameId}")]
        public ActionResult<Game> UpdateGame(int gameId, [FromBody] Game updatedGame)
        {
            Game game = _gameRepository.GetGameById(gameId);
            if (game == null)
                return NotFound();

            // Mettre à jour la partie de Mastermind avec les données fournies
            game.SecretCode = updatedGame.SecretCode;
            game.IsGameOver = updatedGame.IsGameOver;

            _gameRepository.UpdateGame(game);

            return Ok(game);
        }

        [HttpDelete("{gameId}")]
        public ActionResult DeleteGame(int gameId)
        {
            Game game = _gameRepository.GetGameById(gameId);
            if (game == null)
                return NotFound();

            _gameRepository.DeleteGame(game);

            return NoContent();
        }
    }
}
