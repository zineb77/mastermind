using System;

namespace MastermindAPI.Models
{
    public class Game
    {
        public int Id { get; set; }
        public string SecretCode { get; set; }
        public int MaxAttempts { get; set; }
        public int RemainingAttempts { get; set; }
        public bool IsGameOver { get; set; }

        public Game()
        {
            Id = GenerateGameId();
            SecretCode = GenerateSecretCode();
            MaxAttempts = 10;
            RemainingAttempts = MaxAttempts;
            IsGameOver = false;
        }

        private int GenerateGameId()
        {
            // Générer un identifiant de jeu unique (par exemple, en utilisant un compteur ou une logique spécifique)
            return new Random().Next(1000, 9999);
        }

        private string GenerateSecretCode()
        {
            // Générer un code secret aléatoire (par exemple, en utilisant une logique spécifique)
            // Retourner le code secret généré
            return "2001";
        }
    }
}
